$(document).ready(function () {
    $('.authors_slider').slick({
        arrows: true,
        slidesToShow: 4,
        speed: 1000,
        easing: "ease",
        focusOnSelect: true,
        asNavFor: ".comments_slider",
        centerPadding: 0,
        centerMode: true,
    });

    $('.comments_slider').slick({
        arrows: false,
        slidesToShow: 1,
        speed: 1000,
        infinite: true,
        focusOnSelect: true,
        asNavFor: ".authors_slider",
        fade: true,
        cssEase: 'ease'
    });
});