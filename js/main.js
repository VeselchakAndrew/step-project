// tabs

const tabsContainer = document.querySelector(".division_tabs_content");
const buttonsContainer = document.querySelector(".division_tabs");
const buttons = buttonsContainer.querySelectorAll("li");
const tabs = tabsContainer.querySelectorAll("li");

function addDataAttr() {
	const activeButton = document.querySelector(
		".division_tabs > li:first-child"
	);
	const activeTab = document.querySelector(
		".division_tabs_content > li:first-child"
	);
	activeButton.classList.add("active_tab");
	activeTab.classList.add("division_tab_active");

	for (let i = 0; i < buttons.length; i++) {
		if (!buttons[i].classList.contains("division_tab")) {
			buttons[i].classList.add("division_tab");
		}
	}

	for (let i = 0; i < tabs.length; i++) {
		if (!tabs[i].classList.contains("division_tab_content")) {
			tabs[i].classList.add("division_tab_conten");
		}
	}

	for (let i = 0; i < buttons.length; i++) {
		buttons[i].setAttribute("data-for-tab", `${i}`);
	}

	for (let i = 0; i < tabs.length; i++) {
		tabs[i].setAttribute("data-tab", `${i}`);
	}
}

function jsTabs(button) {
	const tabNumber = button.dataset.forTab;
	const tabToActive = tabsContainer.querySelector(
		`.division_tab_content[data-tab="${tabNumber}"]`
	);

	buttons.forEach((button) => {
		button.classList.remove("active_tab");
	});

	tabs.forEach((tab) => {
		tab.classList.remove("division_tab_active");
	});

	button.classList.add("active_tab");
	tabToActive.classList.add("division_tab_active");
}

buttons.forEach((button) => {
	button.addEventListener("click", () => {
		jsTabs(button);
	});
});

document.addEventListener("DOMContentLoaded", () => {
	addDataAttr();
});

// filter
const categorySelectors = document.querySelectorAll(".our_works__button");
const graphicDesign = document.querySelectorAll(".graphic_design");
const webDesign = document.querySelectorAll(".web_design");
const landingPages = document.querySelectorAll(".landing_pages");
const wordpress = document.querySelectorAll(".wordpress");
const all = document.querySelectorAll(".our_works__img_container");

const btnLoadMore = document.querySelector(".more");
const loader = document.querySelector(".holder");

let randomIndex = [];
let clickCount = 0;

function randomLoadingCards(countOfImage) {
	while (randomIndex.length < countOfImage) {
		let random = Math.floor(Math.random() * all.length);
		if (randomIndex.indexOf(random) === -1) {
			randomIndex.push(random);
		}
	}
	for (let i = 0; i < countOfImage; i++) {
		all[randomIndex[i]].classList.add("show");
	}
}

function showBtn() {
	if (btnLoadMore.classList.contains("remove")) {
		btnLoadMore.classList.remove("remove");
	}
}

function hideBtn() {
	if (!btnLoadMore.classList.contains("remove")) {
		btnLoadMore.classList.add("remove");
	}
}

categorySelectors.forEach((selector) => {
	selector.addEventListener("click", () => {
		hideBtn();
		categorySelectors.forEach((selector) => {
			selector.classList.remove("checked");
		});

		selector.classList.add("checked");

		let dataCategory = selector.getAttribute("data-category");
		console.log(dataCategory);

		all.forEach((item) => {
			item.classList.remove("show");
		});

		if (dataCategory === "graphic_design") {
			graphicDesign.forEach((item) => {
				item.classList.add("show");
			});
		} else if (dataCategory === "web_design") {
			webDesign.forEach((item) => {
				item.classList.add("show");
			});
		} else if (dataCategory === "landing_pages") {
			landingPages.forEach((item) => {
				item.classList.add("show");
			});
		} else if (dataCategory === "wordpress") {
			wordpress.forEach((item) => {
				item.classList.add("show");
			});
		} else {
			for (let i = 0; i < 12; i++) {
				all[randomIndex[i]].classList.add("show");
				showBtn();
			}
		}
	});
});

document.addEventListener("DOMContentLoaded", () => {
	randomLoadingCards(12);
});

function showLoader() {
	btnLoadMore.classList.add("remove");
	loader.classList.remove("remove");
	setTimeout(function () {
		loader.classList.add("remove");
		btnLoadMore.classList.remove("remove");
	}, 2000);
}

btnLoadMore.addEventListener("click", () => {
	showLoader();
	setTimeout(function () {
		if (clickCount === 0) {
			randomLoadingCards(24);
		} else {
			randomLoadingCards(36);
			btnLoadMore.classList.add("remove");
		}

		clickCount++;
	}, 2000);
});

//----------------------------
